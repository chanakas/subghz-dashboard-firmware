/* @file CustomKeypad.pde
|| @version 1.0
|| @author Alexander Brevig
|| @contact alexanderbrevig@gmail.com
||
|| @description
|| | Demonstrates changing the keypad size and key values.
|| #
*/
#include <TimerOne.h>
#include <Keypad.h>
#include <Adafruit_NeoPixel.h>
#include <RingBufCPP.h>

#ifdef __AVR__
  #include <avr/power.h>
#endif

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN            11
#define BUZZER_PIN 12
// How many NeoPixels are attached to the Arduino?
#define NUM_BUTTONS 14
#define NUMPIXELS      18
#define RED_INTENSITY 35
#define GREEN_INTENSITY 35
#define BLUE_INTENSITY 35
#define MAX_NUM_ELEMENTS_MQTT 20
#define MAX_NUM_ELEMENTS 20
// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.

//SEVEN SEGMENT SECTION

#include <Thread.h>

#define LATCH A2
#define CLK A1
#define DATA A0

//This is the hex value of each number stored in an array by index num
byte digits[10]= {0b00111111, 0b00000110, 0b01011011, 0b01001111, 0b01100110, 0b01101101, 0b01111101 , 0b00000111, 0b01111111, 0b01101111};
byte select[2]= {0b00000001, 0b00000010};
int digitOne;
int digitTwo;
int tempBedNumber = 0;
//My simple Thread
bool isFirstDigit = 0;
Thread myThread = Thread();

// callback for myThread
void niceCallback(){
  if(isFirstDigit == 0){
    isFirstDigit = 1;
    digitalWrite(LATCH, LOW);
      shiftOut(DATA, CLK, MSBFIRST, select[0]); // digitTwo
      shiftOut(DATA, CLK, MSBFIRST, ~digits[digitOne]); // digitOne
      digitalWrite(LATCH, HIGH);
     // delay(1);
  }
  else{
     isFirstDigit = 0;
      digitalWrite(LATCH, LOW);
      shiftOut(DATA, CLK, MSBFIRST, select[1]); // digitTwo
      shiftOut(DATA, CLK, MSBFIRST, ~digits[digitTwo]); // digitOne
      digitalWrite(LATCH, HIGH);
      //delay(1);
    }

}






const byte ROWS = 3; //four rows
const byte COLS = 9; //four columns
int NeoPixelState[NUMPIXELS];
int isPixelsTurnedOn = 0;
volatile long secondCount = 0;
long lastHearbeat[NUMPIXELS];
boolean isLive[NUMPIXELS];
String inputString = "";         // a string to hold incoming data
//define the cymbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] = {
  {'A','B','C','D','E','F'},
  {'G','H','I','J','K','L'},
  {'M','N','O','P','Q','R'},
};
byte rowPins[ROWS] = {4, 3, 2}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {10, 9, 8, 7, 6, 5}; //connect to the column pinouts of the keypad

//initialize an instance of class NewKeypad
Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS); 

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

int delayval = 500; // delay for half a second


struct Event
{
  int sensorID;
  int state;
  unsigned long timestamp;
};
struct Event temp;
struct Event temp2;
struct Event currentDisplay;
RingBufCPP<struct Event, MAX_NUM_ELEMENTS_MQTT> mqttBuf;
RingBufCPP<struct Event, MAX_NUM_ELEMENTS> tempBuf;
RingBufCPP<struct Event, MAX_NUM_ELEMENTS> sevenSegBuf;
void serialEventRun(void) {
  
     while (Serial.available() > 0) {
    
      // get the new byte:
    char inChar = (char)Serial.read();
    if (inChar == 'S' || inChar == 'H' ){
    if(inChar == 'S')
      temp.state = 1;
    else
      temp.state = 2;
     inChar = (char)Serial.read();
     while(inChar != '0'){
        inChar = (char)Serial.read();
      }
      inputString = "";
    }
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      temp.sensorID = inputString.toInt() -1;
      temp.timestamp = secondCount;

      //add to the FIFO
      for(int i = 0; i < tempBuf.numElements(); i++){
           Event *e = tempBuf.peek(i);
           if((temp.timestamp - (*e).timestamp) > 5){
              tempBuf.pull(&temp2);
              }
      }
      if (tempBuf.isEmpty()){
        mqttBuf.add(temp);
        tempBuf.add(temp);
      
        }
      else if (!tempBuf.isFull())
      {
        bool isUnique = 1;
        for(int i = 0; i < tempBuf.numElements(); i++){
           Event *e = tempBuf.peek(i);
           if((*e).sensorID == temp.sensorID && temp.state == (*e).state){
              isUnique = 0;
            }
        }
        if(isUnique){
          mqttBuf.add(temp);
          tempBuf.add(temp);  

        }
      }
         
      inputString = "";
    }   
  }

}

void blinkLED(void)
{
secondCount++;
if(isPixelsTurnedOn){
  isPixelsTurnedOn = 0;
}else
{
  isPixelsTurnedOn = 1;
}
for (int i = 0; i < NUMPIXELS; i++)
{
	 if(myThread.shouldRun() && NeoPixelState[currentDisplay.sensorID] == 3){

     digitOne = tempBedNumber/10;
     digitTwo = tempBedNumber%10;
    myThread.run();

  }
  if(isLive[i] == 0 ){
    if(!isPixelsTurnedOn){
      if(NeoPixelState[i] != 3)
          setPixelColorYellow(i);
        
      }else
      {
        if(NeoPixelState[i] !=3)
          setPixelOff(i);
        
      }
    }

}

}

void setup(){
  Serial.begin(9600);
  pinMode(BUZZER_PIN, OUTPUT);
  digitalWrite(BUZZER_PIN, HIGH);
  Timer1.initialize(1000000);
  Timer1.attachInterrupt(blinkLED); // blinkLED to run every 0.15 seconds
  pixels.begin(); // This initializes the NeoPixel library.

  pinMode(LATCH, OUTPUT);
  pinMode(CLK, OUTPUT);
  pinMode(DATA, OUTPUT);
  myThread.onRun(niceCallback);
  myThread.setInterval(1);

  currentDisplay.sensorID = -1;
}

void checkHeartbeat(){
  for (int i = 0; i < NUMPIXELS; i++)
  {
    if((secondCount - lastHearbeat[i]) > 180){
      isLive[i] = 0;

    }else
    {
      if(NeoPixelState[i] == 0)
        setPixelOff(i);
      else if(NeoPixelState[i] == 3)
        setPixelColorRed(i);
      isLive[i] = 1;
      
    }
  }
}
void ringBuzeer(){
  digitalWrite(BUZZER_PIN, LOW);
  delay(50);
  digitalWrite(BUZZER_PIN, HIGH);
  delay(20);
  digitalWrite(BUZZER_PIN, LOW);
  delay(100);
  digitalWrite(BUZZER_PIN, HIGH);
  delay(20);
  digitalWrite(BUZZER_PIN, LOW);
  delay(100);
  digitalWrite(BUZZER_PIN, HIGH);
}
  
void loop(){
  char customKey = customKeypad.getKey();
  
  if (customKey){
    Serial.println(customKey);
  }
    int temp = customKey - 'A';
    if(NeoPixelState[temp] == 3){
      setPixelOff(temp);
      NeoPixelState[temp] = 0;
    }

  serialEventRun();

   if(mqttBuf.numElements()>0){
        while (mqttBuf.pull(&temp2))
        {
        
          if(temp2.state == 2){
            lastHearbeat[temp2.sensorID] = secondCount;
          
          }
          if(temp2.state == 1){
           sevenSegBuf.add(temp2);
           setPixelColorRed(temp2.sensorID);
           NeoPixelState[temp2.sensorID] = 3;
           lastHearbeat[temp2.sensorID] = secondCount;
           ringBuzeer();
        }
         
        }
    }
    if(sevenSegBuf.numElements()>0 || NeoPixelState[currentDisplay.sensorID] == 3){
    	if(currentDisplay.sensorID == -1){
    	   sevenSegBuf.pull(&currentDisplay);
    	}
    	if(NeoPixelState[currentDisplay.sensorID] == 3){
  		   tempBedNumber = currentDisplay.sensorID + 1;
    	}else{
    	   sevenSegBuf.pull(&currentDisplay);
    	}
    	
    }
    checkHeartbeat();
      if(myThread.shouldRun() && NeoPixelState[currentDisplay.sensorID] == 3){
    //tempBedNumber = 45;
     digitOne = tempBedNumber/10;
     digitTwo = tempBedNumber%10;
    myThread.run();

  }
  if(NeoPixelState[currentDisplay.sensorID] != 3){
  	clearSevenSeg();
  }
}

void clearSevenSeg(){
	 digitalWrite(LATCH, LOW);
      shiftOut(DATA, CLK, MSBFIRST, 0); // digitTwo
      shiftOut(DATA, CLK, MSBFIRST, ~digits[digitOne]); // digitOne
      digitalWrite(LATCH, HIGH);
}

void setPixelColorRed(int pixelNumber){
  if(pixelNumber < NUM_BUTTONS){
  pixels.setPixelColor(pixelNumber , pixels.Color(RED_INTENSITY,0,0)); // Moderately bright green color.

    pixels.show(); // This sends the updated pixel color to the hardware.
  }
    //delay(50); // Delay for a period of time (in milliseconds).
}

void setPixelColorGreen(int pixelNumber){
  if(pixelNumber < NUM_BUTTONS){
  pixels.setPixelColor(pixelNumber , pixels.Color(0,GREEN_INTENSITY,0)); // Moderately bright green color.

    pixels.show(); // This sends the updated pixel color to the hardware.
  }
   // delay(50); // Delay for a period of time (in milliseconds).
}

void setPixelColorYellow(int pixelNumber){
  if(pixelNumber < NUM_BUTTONS){
  pixels.setPixelColor(pixelNumber , pixels.Color(RED_INTENSITY,GREEN_INTENSITY,0)); // Moderately bright green color.

    pixels.show(); // This sends the updated pixel color to the hardware.
  }
  //  delay(50); // Delay for a period of time (in milliseconds).
}

void setPixelOff(int pixelNumber){
  if(pixelNumber < NUM_BUTTONS){
  pixels.setPixelColor(pixelNumber , pixels.Color(0,0,0)); // Moderately bright green color.

    pixels.show(); // This sends the updated pixel color to the hardware.
  }
//delay(50); // Delay for a period of time (in milliseconds).

}